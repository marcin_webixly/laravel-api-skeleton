<?php

namespace App\Foundation\Framework\Http\Controllers;

use App\Foundation\Framework\Serializers\ApiDataSerializer;
use Illuminate\Http\JsonResponse;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use ReflectionClass;

class ApiController extends Controller
{
    protected int $statusCode = 200;

    const CODE_WRONG_ARGS = 'GEN-FUBARGS';
    const CODE_NOT_FOUND = 'GEN-LIKETHEWIND';
    const CODE_INTERNAL_ERROR = 'GEN-AAAGGH';
    const CODE_UNAUTHORIZED = 'GEN-MAYBGTFO';
    const CODE_FORBIDDEN = 'GEN-GTFO';
    const CODE_INVALID_MIME_TYPE = 'GEN-UMWUT';

    protected $transformerClass = null;

    protected ?TransformerAbstract $transformer = null;

    protected Manager $fractal;

    public function __construct()
    {
        $this->fractal = new Manager;
        $this->fractal->setSerializer(new ApiDataSerializer());

        if ($this->transformerClass !== null && class_exists($this->transformerClass)) {
            $this->transformer = new $this->transformerClass();
        }
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function noContent(): JsonResponse
    {
        return response()->json(null, 204);
    }

    public function success(string $message = 'Successfully', array $additionalData = []): JsonResponse
    {
        return $this->setStatusCode(200)->respondWithArray(array_merge([
            'success' => true,
            'message' => $message,
        ], $additionalData));
    }

    public function respondWithItem($item, $callback, $message = null): mixed
    {
        $key = strtolower((new ReflectionClass($item))->getShortName());
        $data[$key] = $this->createItem($item, $callback);
        if ($message !== null) {
            $data['message'] = $message;
        }

        return $this->respondWithArray($data);
    }

    protected function createItem($item, $callback): array
    {
        $resource = new Item($item, $callback);
        return $this->fractal->createData($resource)->toArray()['data'];
    }

    public function respondWithCollection($collection, $callback, $message = null): mixed
    {
        $resource = new Collection($collection, $callback);

        $data = $this->fractal->createData($resource)->toArray();
        if ($message !== null) {
            $data['message'] = $message;
        }

        return $this->respondWithArray($data);
    }

    public function respondWithPaginator($paginator, $callback, $message = 'Successfully'): mixed
    {
        $resource = new Collection($paginator->getCollection(), $callback);

        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        $data = $this->fractal->createData($resource)->toArray();
        $data['message'] = $message;

        return $this->respondWithArray($data);
    }

    public function respondWithArray(array $array, array $headers = []): JsonResponse
    {
        return response()->json($array, $this->statusCode, $headers);
    }

    public function respondWithMessage(string $message): JsonResponse
    {
        return $this->setStatusCode(200)
            ->respondWithArray([
                'message' => $message,
            ]);
    }

    protected function respondWithError(string $message, string $errorCode, array $errors = []): JsonResponse
    {
        if ($this->statusCode === 200) {
            trigger_error(
                "You better have a really good reason for erroring on a 200...",
                E_USER_WARNING
            );
        }

        return $this->respondWithArray([
            'success' => false,
            'errors' => $errors,
            'code' => $errorCode,
            'message' => $message,
        ]);
    }

    public function errorForbidden(string $message = 'Forbidden', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(500)
            ->respondWithError($message, self::CODE_FORBIDDEN, $errors);
    }

    public function errorInternalError(string $message = 'Internal Error', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(500)
            ->respondWithError($message, self::CODE_INTERNAL_ERROR, $errors);
    }

    public function errorNotFound(string $message = 'Resource Not Found', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(404)
            ->respondWithError($message, self::CODE_NOT_FOUND, $errors);
    }

    public function errorUnauthorized(string $message = 'Unauthorized', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(401)
            ->respondWithError($message, self::CODE_UNAUTHORIZED, $errors);
    }

    public function errorWrongArgs(string $message = 'Bad request', array $errors = []): JsonResponse
    {
        return $this->setStatusCode(400)
            ->respondWithError($message, self::CODE_WRONG_ARGS, $errors);
    }
}
