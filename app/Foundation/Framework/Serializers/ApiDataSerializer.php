<?php

namespace App\Foundation\Framework\Serializers;

use League\Fractal\Serializer\ArraySerializer;

class ApiDataSerializer extends ArraySerializer
{
    /**
     * {@inheritDoc}
     */
    public function collection(?string $resourceKey, array $data): array
    {
        if ($resourceKey == 'relationship') {
            return $data;
        }

        return ['data' => $data];
    }

    /**
     * {@inheritDoc}
     */
    public function item(?string $resourceKey, array $data): array
    {
        return ['data' => $data];
    }

    /**
     * {@inheritDoc}
     */
    public function null(): ?array
    {
        return ['data' => []];
    }
}
