<?php

namespace App\Foundation\Framework\Providers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public function register(): void
    {
        //
    }

    public function boot(): void
    {
        $this->emailsAlwaysToForLocal();
    }

    private function emailsAlwaysToForLocal(): void
    {
        if (!$this->app->environment('production')) {
            Mail::alwaysTo(config('mail.always_to'));
        }
    }
}
