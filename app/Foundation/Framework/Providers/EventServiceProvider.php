<?php

namespace App\Foundation\Framework\Providers;

use App\Foundation\Auth\Events\PasswordChanged;
use App\Foundation\Auth\Events\VerifiedNewAccount;
use App\Foundation\Auth\Listeners\LogoutFromAllSessions;
use App\Foundation\Auth\Listeners\SendPasswordChangedNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use Stancl\JobPipeline\JobPipeline;

class EventServiceProvider extends ServiceProvider
{

    protected $listen = [];

    public function events(): array
    {
        return [
            Registered::class => [],
            VerifiedNewAccount::class => [
                JobPipeline::make([
                    \App\Foundation\Tenancy\Jobs\CreateTenant::class,
                ])->send(function (VerifiedNewAccount $event) {
                    return $event->user;
                })->shouldBeQueued(false),
                \App\Foundation\Auth\Listeners\SendWelcomeMessage::class
            ],
            PasswordChanged::class => [
                LogoutFromAllSessions::class,
                SendPasswordChangedNotification::class
            ]
        ];
    }

    public function boot()
    {
        $this->bootEvents();
    }

    public function bootEvents()
    {
        foreach ($this->events() as $event => $listeners) {
            foreach ($listeners as $listener) {
                if ($listener instanceof JobPipeline) {
                    $listener = $listener->toListener();
                }

                Event::listen($event, $listener);
            }
        }
    }
}
