<?php namespace App\Foundation\Auth\Transformers;

use App\Foundation\Auth\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user): array
    {
        return [
            'id'    => $user->id,
            'email' => $user->email,
            'email_verified_at' => $user->email_verified_at,
            'tenant_id' => $user->tenant_id
        ];
    }
}
