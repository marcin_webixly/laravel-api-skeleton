<?php

namespace App\Foundation\Auth\Mails;

use App\Foundation\Auth\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class UserWelcome extends Mailable
{
    use Queueable, SerializesModels;

    protected User $user;


    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build(): self
    {
        return $this
            ->subject(__('Your :appName account has been activated!', [
                'appName' => config('app.name')
            ]))
            ->markdown('emails.users.welcome');
    }
}
