<?php

namespace App\Foundation\Auth\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordChanged extends Notification implements ShouldQueue
{
    use Queueable;

    public function via(): array
    {
        return ['mail'];
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage)
            ->subject('Twoje hasło zostało zmienione')
            ->greeting('Twoje hasło zostało zmienione.')
            ->line('Zgodnie z Twoją prośbą zmieniliśmy hasło do aplikacji ' . config('app.name') . '.')
            ->line('Jeśli prośba nie pochodzi od Ciebie - skontaktuj się z nami!');
    }
}
