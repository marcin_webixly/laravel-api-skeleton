<?php

namespace App\Foundation\Auth\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NextApps\VerificationCode\Notifications\VerificationCodeCreatedInterface;

class VerificationCodeCreated extends Notification implements ShouldQueue, VerificationCodeCreatedInterface
{
    use Queueable;

    public string $code;

    public function __construct(string $code)
    {
        $this->code = $code;
    }

    public function via(): array
    {
        return ['mail'];
    }

    private function easyReadCode(): string
    {
        return substr($this->code, 0, 3) . ' ' . substr($this->code, 3, 3);
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage())
            ->subject(__('Kod weryfikacyjny: ') . $this->easyReadCode())
            ->markdown('notifications.auth.verificationCodeCreated', [
                'code' => $this->code
            ]);
    }
}
