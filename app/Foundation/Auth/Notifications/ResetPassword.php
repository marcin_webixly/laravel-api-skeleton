<?php

namespace App\Foundation\Auth\Notifications;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordCore;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class ResetPassword extends ResetPasswordCore
{
    protected function buildMailMessage($url): MailMessage
    {
        return (new MailMessage)
            ->subject('Resetowanie hasła do konta')
            ->greeting('Resetowanie hasła do konta')
            ->line(
                "Otrzymaliśmy prośbę o zresetowanie hasła do konta w aplikacji " . config('app.name') . ". " .
                Lang::get("Kliknij poniżej w ciągu :count minut, aby przejść do aplikacji i\nzmienić hasło.", [
                    'count' => config('auth.passwords.'.config('auth.defaults.passwords').'.expire')
                ])
            )
            ->action(Lang::get('Zmień hasło'), $url);
    }
}
