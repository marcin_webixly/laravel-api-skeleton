<?php

namespace App\Foundation\Auth\Listeners;

use App\Foundation\Auth\Events\VerifiedNewAccount;
use App\Foundation\Auth\Mails\UserWelcome;
use Illuminate\Support\Facades\Mail;

class SendWelcomeMessage
{
    public function handle(VerifiedNewAccount $event): void
    {
        Mail::to($event->user)->send(new UserWelcome($event->user));
    }
}
