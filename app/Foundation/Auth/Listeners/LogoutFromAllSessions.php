<?php

namespace App\Foundation\Auth\Listeners;

use App\Foundation\Auth\Events\PasswordChanged;

class LogoutFromAllSessions
{
    public function handle(PasswordChanged $passwordChanged): void
    {
        $ignoreTokens = ($passwordChanged->user->token() !== null) ? [$passwordChanged->user->token()->id] : [];
        $passwordChanged->user->logoutFromAllSessions($ignoreTokens);
    }
}
