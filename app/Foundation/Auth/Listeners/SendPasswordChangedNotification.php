<?php

namespace App\Foundation\Auth\Listeners;

use App\Foundation\Auth\Events\PasswordChanged;
use App\Foundation\Auth\Notifications\PasswordChanged as PasswordChangedNotification;

class SendPasswordChangedNotification
{
    public function handle(PasswordChanged $passwordChanged): void
    {
        $passwordChanged->user->notify(new PasswordChangedNotification());
    }
}
