<?php

namespace App\Foundation\Auth\Events;

use App\Foundation\Auth\Models\User;
use Illuminate\Queue\SerializesModels;

class PasswordChanged
{
    use SerializesModels;


    public User $user;


    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
