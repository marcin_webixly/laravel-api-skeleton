<?php

namespace App\Foundation\Auth\Models;

use App\Foundation\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use App\Foundation\Framework\Traits\UUID;
use App\Foundation\Tenancy\Models\Tenant;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 *
 * @property string $id
 * @property string $email
 * @property string $email_verified_at
 * @property string $tenant_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @mixin \Eloquent
 *
 */
class User extends Authenticatable implements MustVerifyEmail, CanResetPassword
{
    use HasApiTokens, HasFactory, Notifiable, UUID;

    protected $fillable = [
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tenant(): BelongsTo
    {
        return $this->belongsTo(Tenant::class, 'tenant_id', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function logoutFromAllSessions($ignoreTokenIds = []): void
    {
        $this->tokens->each(function ($token) use ($ignoreTokenIds) {
            (!in_array($token->id, $ignoreTokenIds)) && $token->delete();
        });
    }
}
