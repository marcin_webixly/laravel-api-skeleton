<?php

namespace App\Foundation\Auth\Http\Controllers;

use App\Foundation\Auth\Http\Requests\LoginRequest;
use App\Foundation\Auth\Transformers\UserTransformer;
use App\Foundation\Framework\Http\Controllers\ApiController;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Validation\ValidationException;

class LoginController extends ApiController
{
    public function authenticate(LoginRequest $request): JsonResponse
    {
        $this->ensureIsNotRateLimited($request);

        if (! Auth::attempt($request->only('email', 'password'), $request->boolean('remember'))) {
            RateLimiter::hit($request->throttleKey());

            throw ValidationException::withMessages([
                'email' => __('auth.failed'),
            ]);
        }

        RateLimiter::clear($request->throttleKey());
        $user = auth()->user();
        $token = $user->createToken('Web')->accessToken;

        return $this->success(__('auth.success'), [
            'accessToken' => $token,
            'user' => $this->createItem($user, new UserTransformer())
        ]);
    }

    public function ensureIsNotRateLimited(LoginRequest $request): void
    {
        if (!RateLimiter::tooManyAttempts($request->throttleKey(), 5)) {
            return;
        }

        event(new Lockout($request));

        $seconds = RateLimiter::availableIn($request->throttleKey());

        throw ValidationException::withMessages([
            'email' => trans('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ]),
        ]);
    }

    public function logout(): JsonResponse
    {
        $user = Auth::user();
        $user->token()->revoke();
        $user->token()->delete();

        return $this->noContent();
    }
}
