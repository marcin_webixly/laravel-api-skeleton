<?php

namespace App\Foundation\Auth\Http\Controllers;

use App\Foundation\Auth\Events\PasswordChanged;
use App\Foundation\Auth\Http\Requests\ChangePasswordRequest;
use App\Foundation\Auth\Transformers\UserTransformer;
use App\Foundation\Framework\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends ApiController
{
    public function me(Request $request): JsonResponse
    {
        return $this->respondWithItem($request->user(), new UserTransformer());
    }

    public function changePassword(ChangePasswordRequest $request): JsonResponse
    {
        $user = $request->user();

        $user->update([
            'password' => Hash::make($request->new_password)
        ]);

        event(new PasswordChanged($user));

        return $this->success(__('passwords.changed'));
    }
}
