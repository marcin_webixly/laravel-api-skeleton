<?php

namespace App\Foundation\Auth\Http\Controllers;

use App\Foundation\Auth\Events\VerifiedNewAccount;
use App\Foundation\Auth\Http\Requests\EmailVerificationRequest;
use App\Foundation\Framework\Http\Controllers\ApiController;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\JsonResponse;

class VerifyEmailController extends ApiController
{
    public function __invoke(EmailVerificationRequest $request): JsonResponse
    {
        if ($request->user()->hasVerifiedEmail()) {
            return $this->errorWrongArgs(__('auth.verification.already-verified-error'));
        }

        if ($request->user()->markEmailAsVerified()) {
            ($request->user()->tenant_id === null)
                ? event(new VerifiedNewAccount($request->user()))
                : event(new Verified($request->user()));
        }

        return $this->success(__('auth.verification.success'));
    }
}
