<?php

namespace App\Foundation\Auth\Http\Controllers;

use App\Foundation\Framework\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use NextApps\VerificationCode\VerificationCode;

class EmailVerificationNotificationController extends ApiController
{
    public function store(Request $request): JsonResponse
    {
        if ($request->user()->hasVerifiedEmail()) {
            return $this->errorWrongArgs(__('auth.verification.already-verified-error'));
        }

        VerificationCode::send($request->user()->email);

        return $this->success(__('auth.verification.sent'));
    }
}
