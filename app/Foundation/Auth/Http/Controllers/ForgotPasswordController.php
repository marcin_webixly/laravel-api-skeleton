<?php

namespace App\Foundation\Auth\Http\Controllers;

use App\Foundation\Framework\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends ApiController
{
    public function sendResetEmail(Request $request): JsonResponse
    {
        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        if ($status !== Password::RESET_LINK_SENT) {
            sleep(1);
        }

        return $this->noContent();
    }
}
