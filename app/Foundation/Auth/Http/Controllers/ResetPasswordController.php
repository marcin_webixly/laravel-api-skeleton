<?php

namespace App\Foundation\Auth\Http\Controllers;

use App\Foundation\Auth\Events\PasswordChanged;
use App\Foundation\Auth\Http\Requests\ResetPasswordRequest;
use App\Foundation\Framework\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends ApiController
{
    public function resetPassword(ResetPasswordRequest $request): JsonResponse
    {
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ]);

                $user->save();

                event(new PasswordChanged($user));
            }
        );

        if ($status === Password::PASSWORD_RESET) {
            return $this->success(__($status));
        }

        return $this->errorWrongArgs(__($status), [
            'email' => [__($status)]
        ]);
    }
}
