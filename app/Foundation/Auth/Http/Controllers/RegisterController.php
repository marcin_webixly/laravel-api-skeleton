<?php

namespace App\Foundation\Auth\Http\Controllers;

use App\Foundation\Auth\Http\Requests\RegisterRequest;
use App\Foundation\Auth\Models\User;
use App\Foundation\Auth\Transformers\UserTransformer;
use App\Foundation\Framework\Http\Controllers\ApiController;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class RegisterController extends ApiController
{
    public function register(RegisterRequest $request): JsonResponse
    {
        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        event(new Registered($user));

        return $this->success(__('auth.register.success'), [
            'accessToken' => $user->createToken('Web')->accessToken,
            'user' => $this->createItem($user, new UserTransformer())
        ]);
    }
}
