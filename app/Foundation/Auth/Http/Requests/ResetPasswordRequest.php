<?php

namespace App\Foundation\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 * @property string $email
 * @property string $password
 * @property string $password_confirmation
 * @property string $token
 *
 */

class ResetPasswordRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => [
                'required',
                'confirmed',
                RegisterRequest::passwordRule()
            ],
        ];
    }
}
