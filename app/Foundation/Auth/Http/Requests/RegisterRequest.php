<?php

namespace App\Foundation\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

/**
 *
 * @property string $email
 * @property string $password
 *
 */

class RegisterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => [
                'required',
                self::passwordRule()
            ],
        ];
    }

    public static function passwordRule(): Password
    {
        return Password::min(8)->mixedCase()->numbers()->symbols();
    }
}
