<?php

namespace App\Foundation\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

/**
 *
 * @property string $old_password
 * @property string $new_password
 * @property string $new_password_confirmation
 *
 */

class ChangePasswordRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'old_password' => ['required', function($attribute, $value, $fail) {
                $this->matchOldPassword($attribute, $value, $fail);
            }],
            'new_password' => [
                'required',
                'confirmed',
                RegisterRequest::passwordRule()
            ],
        ];
    }

    private function matchOldPassword($attribute, $value, $fail): void
    {
        if (!Hash::check($value, $this->user()->password)) {
            $fail(__('validation.old_password_match'));
        }
    }
}
