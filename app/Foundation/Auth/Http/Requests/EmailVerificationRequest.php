<?php

namespace App\Foundation\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailVerificationRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'code' => [
                'required',
                'size:6',
//                function($attribute, $value, $fail){
//                    if (!VerificationCode::verify($value, $this->user()->email)) {
//                        $fail('Podany kod weryfikacyjny jest nieprawidłowy');
//                    }
//                }
            ]
        ];
    }
}
