<?php

declare(strict_types=1);

namespace App\Foundation\Tenancy;

use Stancl\Tenancy\Contracts\Tenant;
use Stancl\Tenancy\Resolvers\Contracts\CachedTenantResolver;

class AuthenticatedUserTenantResolver extends CachedTenantResolver
{
    public static $shouldCache = false;

    public static $cacheTTL = 3600; // seconds

    public static $cacheStore = null; // default

    public function resolveWithoutCache(...$args): Tenant
    {
        $tenantId = $args[0];

        if ($tenantId) {
            if ($tenant = tenancy()->find($tenantId)) {
                return $tenant;
            }
        }

        throw new TenantCouldNotBeIdentifiedByAuthenticatedUser($tenantId);
    }

    public function getArgsForTenant(Tenant $tenant): array
    {
        return [
            [$tenant->id],
        ];
    }
}
