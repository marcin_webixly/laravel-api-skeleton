<?php

namespace App\Foundation\Tenancy\Jobs;

use App\Foundation\Auth\Models\User;
use App\Foundation\Tenancy\Models\Tenant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateTenant implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function handle(): void
    {
        $tenant = Tenant::create();
        $this->user->tenant_id = $tenant->id;
        $this->user->save();
    }
}
