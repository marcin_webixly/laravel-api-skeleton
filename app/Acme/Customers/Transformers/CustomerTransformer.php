<?php namespace App\Acme\Customers\Transformers;

use App\Acme\Customers\Models\Customer;
use League\Fractal\TransformerAbstract;

class CustomerTransformer extends TransformerAbstract
{
//    protected array $availableIncludes = [
//        'author'
//    ];

    public function transform(Customer $customer): array
    {
        return [
            'id' => $customer->id,
            'name' => $customer->name,
            'created_at' => $customer->created_at,
            'updated_at' => $customer->updated_at
        ];
    }

//    public function includeAuthor(Book $book): Item
//    {
//        $author = $book->author;
//
//        return $this->item($author, new AuthorTransformer);
//    }
}
