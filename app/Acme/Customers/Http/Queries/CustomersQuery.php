<?php

namespace App\Acme\Customers\Http\Queries;

use App\Acme\Customers\Models\Customer;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CustomersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Customer::query());

        $this->appendFilters();
        $this->allowedSorts('name');
    }

    private function appendFilters(): void
    {
        $this->allowedFilters([
            'name',
            $this->createdAfter(),
            $this->createdBefore()
        ]);
    }

    private function createdBefore(): AllowedFilter
    {
        return AllowedFilter::callback('createdBefore', function (Builder $query, $value) {
            $query->where('created_at', '<=', $this->transformDate($value));
        });
    }

    private function createdAfter(): AllowedFilter
    {
        return AllowedFilter::callback('createdAfter', function (Builder $query, $value) {
            $query->where('created_at', '>=', $this->transformDate($value));
        });
    }

    private function transformDate(string $date): string
    {
        return date('Y-m-d', strtotime($date));
    }
}
