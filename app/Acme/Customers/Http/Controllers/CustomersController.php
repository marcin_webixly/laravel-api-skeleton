<?php

namespace App\Acme\Customers\Http\Controllers;

use App\Acme\Customers\Http\Queries\CustomersQuery;
use App\Acme\Customers\Models\Customer;
use App\Acme\Customers\Transformers\CustomerTransformer;
use App\Foundation\Http\Controllers\Api\V1\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CustomersController extends ApiController
{
    public function index(CustomersQuery $customers): JsonResponse
    {
        return $this->respondWithPaginator($customers->paginate(), new CustomerTransformer());
    }

    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required'
        ]);

        $customer = Customer::create($request->all());
        return $this->respondWithItem($customer, new CustomerTransformer());
    }

    public function update()
    {

    }

    public function delete()
    {

    }
}
