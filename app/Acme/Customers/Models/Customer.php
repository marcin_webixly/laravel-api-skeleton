<?php

namespace App\Acme\Customers\Models;

use App\Foundation\Traits\UUID;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 *
 * @property string $id
 * @property string $name
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @mixin \Eloquent
 *
 */
class Customer extends Model
{
    use UUID;

    protected $fillable = ['name'];

    protected $perPage = 10;
}
