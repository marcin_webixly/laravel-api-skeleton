<?php

use Illuminate\Support\Facades\Route;

Route::prefix('users')->name('users.')->middleware(['auth:api', 'verified'])->group(function () {
    Route::get('me', 'UsersController@me')->name('me')->withoutMiddleware('verified');
    Route::post('me/changePassword', 'UsersController@changePassword')->name('change-password');
});

Route::prefix('auth')->group(function () {

    Route::middleware('guest:api')->group(function () {
        Route::post('register', 'RegisterController@register')->name('register');
        Route::post('login', 'LoginController@authenticate')->name('login');
        Route::post('recovery', 'ForgotPasswordController@sendResetEmail')->name('password.recovery')->middleware('throttle:6,2');
        Route::post('reset', 'ResetPasswordController@resetPassword')->name('password.reset');
    });


    Route::middleware('auth:api')->group(function () {
        Route::post('logout', 'LoginController@logout')->name('logout');
        Route::post('send-verification-code', 'EmailVerificationNotificationController@store')
            ->name('verification.send')->middleware('throttle:6,2');
        Route::post('verify-code', 'VerifyEmailController@__invoke')->name('verification.verify');
    });
});
