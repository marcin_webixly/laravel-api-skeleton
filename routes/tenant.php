<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([
    'api',
    'auth:api',
    'verified',
    \App\Foundation\Tenancy\InitializeTenancyByAuthenticatedUser::class
])->prefix('api/v1')->group(function () {

    Route::prefix('customers')->name('customers.')->namespace('App\Acme\Customers\Http\Controllers')->group(function () {
        Route::get('', 'CustomersController@index')->name('index');
        Route::post('', 'CustomersController@store')->name('store');
    });

});
