@component('mail::message')
# Potwierdź swój adres email

Twój kod potwierdzający znajduje się poniżej — wprowadź go w otwartym oknie przeglądarki, aby zalogować się do aplikacji.

@component('mail::panel', ['alignment' => 'center'])
    <span style="font-size: 21px; font-weight: bold; color: #000">{{ $code }}</span>
@endcomponent

Jeśli prośba o ten e-mail nie pochodzi od Ciebie, nie ma się czym martwić — możesz go bezpiecznie zignorować.

@endcomponent
