@component('mail::message')
# @lang('Your :appName account has been activated!', ['appName' => config('app.name')])

@lang('Thank you for signing up with :appName!', ['appName' => config('app.name')])
@endcomponent
