<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Podany adres email lub hasło są niepoprawne.',
    'password' => 'Podane hasło jest nieprawidłowe.',
    'throttle' => 'Zbyt wiele zapytań. Spróbuj ponownie za :seconds sekund.',
    'success' => 'Poprawnie zalogowano',

    'verification' =>   [
        'already-verified-error' => 'Adres email został już zweryfikowany.',
        'sent' => 'Kod aktywacyjny został wysłany.',
        'success' => 'Twój email został poprawnie zweryfikowany.'
    ],

    'register' => [
        'success' => 'Poprawnie zarejestrowano konto.'
    ]

];
