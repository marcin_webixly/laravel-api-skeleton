<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Twoje hasło zostało zresetowane!',
    'sent' => 'Wysłaliśmy e-mail z linkiem do zresetowania hasła!',
    'throttled' => 'Prosimy odczekać przed ponowną próbą.',
    'token' => 'Nieprawidłowy token.',
    'user' => "Nie możemy znaleźć użytkownika o tym adresie e-mail.",
    'changed' => 'Poprawnie zmieniono hasło.'

];
