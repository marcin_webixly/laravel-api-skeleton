# Multi-tenant Laravel API Skeleton

## Getting stared

1. Configure your database and email driver in the .env configuration file.

2. Install packages with Composer:
   
    ~~~~
    $ composer install
    ~~~~

3. Execute migrations:

    ~~~~
    php artisan migrate
    ~~~~

4. Install Passport:

    ~~~~
    php artisan passport:install
    ~~~~

5. Run artisan server:

    ~~~~
    php artisan serve
    ~~~~

## Used Packages
- laravel/framework: ^8.75 (https://laravel.com/docs/8.x)
- laravel/passport: ^10.4 (https://laravel.com/docs/8.x/passport)
- league/fractal: ^0.20.1 (https://fractal.thephpleague.com/)
- nextapps/laravel-verification-code: ^1.2 (https://github.com/nextapps-be/laravel-verification-code)
- stancl/tenancy: ^3.6 (https://tenancyforlaravel.com/docs/v3/)
- spatie/laravel-query-builder: ^4.0 (https://spatie.be/docs/laravel-query-builder)
